import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

loginClick = () => {
    this.props.navigation.navigate('Register')
}

export default class Login extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={(    this.props.navigation.navigate('Register') )}>
                    <Text>Register</Text>
                </TouchableOpacity>
              <StatusBar style="auto" />
            </View>
          );
    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});