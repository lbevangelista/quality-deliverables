import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, Image, TextInput, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';

function LoginScreen({ navigation }){
  const [username,setUsername] = useState('');
  const [password,setPassword] = useState('');

  return (
    <Animatable.View animation='bounceIn' style={styles.container}>
    <View>
      <Image source={{ uri: 'https://image.flaticon.com/icons/png/512/242/242452.png' }} style={ styles.logo } /> 
      <Text style={ styles.headerText }>Log in to OmniEats</Text>
    </View>

    <View>
      <TextInput style={styles.input} value={username} onChangeText={text => setUsername(text)} placeholder="Username" />
      <TextInput style={styles.input} value={password} onChangeText={text => setPassword(text)}  secureTextEntry={true} placeholder="Password" />
    </View>

    <View>
      <TouchableOpacity style={ styles.loginButton }>
        <Text style={ styles.loginTextButton }>Log in</Text>
      </TouchableOpacity>
    </View>

    <View>
      <TouchableOpacity style={ styles.tapRegister } onPress={() => navigation.navigate('Register')}>
       <Text> New Here? <Text style={ styles.register }>Register</Text> </Text>
      </TouchableOpacity>

      <TouchableOpacity style={ styles.tapRegister }>
       <Text> Trouble Singing In? <Text style={ styles.forgotPW }>Forgot Password</Text> </Text>
      </TouchableOpacity>

    </View>
    <StatusBar style="auto" />

  </Animatable.View>
  )
}

function RegisterScreen(){
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPW, setConfirmPW] = useState('');

  return (
    <View style={ styles.container }>
      <View>
        <Text style={styles.registerHeader}>Create an Account</Text>
        <TextInput style={styles.registerInput} value={firstName} onChangeText={text => setFirstName(text)} placeholder="First Name" />
        <TextInput style={styles.registerInput} value={lastName} onChangeText={text => setLastName(text)} placeholder="Last Name" />
        <TextInput style={styles.registerInput} value={email} onChangeText={text => setEmail(text)} placeholder="Email Address" />
        <TextInput style={styles.registerInput} value={username} onChangeText={text => setUsername(text)} placeholder="Username" />
        <TextInput style={styles.registerInput} value={password} onChangeText={text => setPassword(text)} secureTextEntry={true} placeholder="Password" />
        <TextInput style={styles.registerInput} value={confirmPW} onChangeText={text => setConfirmPW(text)} secureTextEntry={true} placeholder="Confirm Password" />
      </View>

      <View>
        <TouchableOpacity style={styles.registerButton}>
          <Text style={{color: 'white' }}>Register</Text>
        </TouchableOpacity>
      </View>

      <View>
      <Text style={{color: 'red'}}> Requirements: </Text>
      <Text>*None of the fields may be left blank</Text>
      <Text>*Password must 12 characters long</Text>
      <Text>*Password must conatin a capital letter, number, and a special character</Text>
      </View>

      </View>

  )
}
const Stack = createStackNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="OmniEats">
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  logo: {
    width: 305,
    height: 159,
    top: '-70%',
  },
  
  headerText: {
    fontSize: 30,
    textAlign: 'center',
    top: '-60%',
  },

  input: {
    paddingLeft: 10,
    borderColor: '#000',
    borderWidth: 0.3,
    borderStyle: 'solid',
    borderRadius: 10,
    marginBottom: 10,
    height: 50,
    width: 250,
    top: '-80%',
  },

  loginButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 180,
    height: 50,
    borderWidth: 1,
    borderRadius: 50,
    backgroundColor: '#2a9df4',
    borderColor: '#2a9df4',
    top: '-130%',
  },

  loginTextButton: {
    color: 'white',
  },

  tapRegister: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  register: {
    color: 'blue',
  },

  forgotPW: {
    color: 'red',
  },

  registerHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    bottom: 80,
    textAlign: 'center',
  },

  registerInput: {
    paddingLeft: 10,
    borderColor: '#000',
    borderWidth: 0.3,
    borderStyle: 'solid',
    borderRadius: 10,
    marginBottom: 10,
    height: 35,
    width: 250,
    top: "-20%",
  },

  registerButton: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 180,
    height: 50,
    borderWidth: 1,
    borderRadius: 40,
    backgroundColor: '#2a9df4',
    borderColor: '#2a9df4',
    bottom: 40,
  },
});
